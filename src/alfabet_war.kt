fun alphabetWar(fight: String): String {
    val afterBombing = bomb(fight)
    val result = calc(afterBombing)
    return when {
        result.first > result.second -> "Left side wins!"
        result.first < result.second -> "Right side wins!"
        else -> "Let's fight again!"
    }
}

private fun bomb(fight: String) =
    if (!fight.contains("*")) {
        fight
    } else {
        val list = fight.split("*")
        list.mapIndexed { i, s ->
            if (s.length > 1) {
                when (i) {
                    0 -> s.substring(0, s.length - 1)
                    list.size - 1 -> s.substring(1)
                    else -> s.substring(1, s.length - 1)
                }
            } else {
                ""
            }
        }.joinToString("")
    }

private fun calc(fight: String) =
    fight
        .split("")
        .map { s ->
            when (s) {
                "w" -> Pair(4, 0)
                "p" -> Pair(3, 0)
                "b" -> Pair(2, 0)
                "s" -> Pair(1, 0)
                "m" -> Pair(0, 4)
                "q" -> Pair(0, 3)
                "d" -> Pair(0, 2)
                "z" -> Pair(0, 1)
                else -> Pair(0, 0)
            }
        }
        .reduce { acc, pair -> Pair(acc.first + pair.first, acc.second + pair.second) }

fun main() {
    println(alphabetWar("z"))
    println(alphabetWar("****"))
    println(alphabetWar("z*dq*mw*pb*s"))
    println(alphabetWar("zdqmwpbs"))
    println(alphabetWar("zz*zzs"))
    println(alphabetWar("sz**z**zs"))
    println(alphabetWar("z*z*z*zs"))
    println(alphabetWar("*wwwwww*z*"))
}